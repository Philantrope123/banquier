/*
 * Renaud Nathan - 20072913
 * Guilbaud Juliette - 20066898
 */

/* This `define` tells unistd to define usleep and random.  */
#define _XOPEN_SOURCE 500

#include "client_thread.h"
#include <netinet/in.h>

int port_number = -1;
int num_request_per_client = -1;
int num_resources = -1;

int *provisioned_resources = NULL;
int **last_request;
int **ressources_need;
int **ressources_actual;

// Access ints
int begun = 0;
int initiated = 0;

// Mutex
pthread_mutex_t lock;

// Variable d'initialisation des threads clients.
unsigned int count = 0;

// Variable du journal.
// Nombre de requête acceptée (ACK reçus en réponse à REQ)
unsigned int count_accepted = 0;

// Nombre de requête en attente (WAIT reçus en réponse à REQ)
unsigned int count_on_wait = 0;

// Nombre de requête refusée (REFUSE reçus en réponse à REQ)
unsigned int count_invalid = 0;

// Nombre de client qui se sont terminés correctement (ACK reçu en réponse à END)
unsigned int count_dispatched = 0;

// Nombre total de requêtes envoyées.
unsigned int request_sent = 0;

// Vous devez modifier cette fonction pour faire l'envoie des requêtes
// Les ressources demandées par la requête doivent être choisies aléatoirement
// (sans dépasser le maximum pour le client). Elles peuvent être positives
// ou négatives.
// Assurez-vous que la dernière requête d'un client libère toute les ressources
// qu'il a jusqu'alors accumulées.
int
send_request (int client_id, int request_id, int socket_fd, int response)
{
    // Receive buffer
    int recv_buff[64];

    // Dummy request
    int request[3+num_resources];
    request[0] = 3;
    request[1] = num_resources+1;
    request[2] = client_id;

    // On regarde si c'est la derniere requete, si oui on libere toutes les ressources
    if (request_id != (num_request_per_client-1) && response == 4) {
        // Random request
        for (int i=0; i<num_resources; i++) {
            int random = (rand() % (ressources_need[client_id][i]+1)) - ressources_actual[client_id][i];
            request[i+3] = random;
            last_request[client_id][i] = random;
        }
    // Si un WAIT a ete recu on renvoie la derniere requete
    } else if (response == 5) {
        for (int i=0; i<num_resources; i++) {
            request[i+3] = last_request[client_id][i];
        }
    } else {
        // Libere toutes les ressources
        for (int i=0; i<num_resources; i++) {
            request[i+3] = 0 - ressources_actual[client_id][i];
        }
    }

    // Send request to server
    if(send(socket_fd, request, sizeof(request), 0) < 0) {
        perror("send()");
    }

    // Feedback
    fprintf (stdout, "Thread %d requests : %d, %d, %d, %d, %d\n", client_id, request[3], request[4], request[5], request[6], request[7]);

    // Recevoir la reponse du server apres la requete
    while(recv(socket_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}

    return recv_buff[0];   
}


void *
ct_code (void *param)
{
    // socket param
    int client_socket_fd = -1;
    client_thread *ct = (client_thread *) param;

    // buffer
    int recv_buff[64];

    // creation du socket client pour communiquer avec le serveur
    client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    // gestion erreur socket creation
    if (client_socket_fd < 0) {
        perror("socket creation");
    }

    // Adresse serveur
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);//(INADDR_LOOPBACK);
    serv_addr.sin_port = htons(port_number);

    // Race to begin and conf
    pthread_mutex_lock(&lock);

    if (!begun) {
        // BEGIN request
        int request_begin[3] = {0,1,count};

        // connect to server socket
        if(connect(client_socket_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
            perror("connect");
        }

        // Send first request to server (BEGIN) {0,1,NB_Clients}
        if(send(client_socket_fd, request_begin, sizeof(request_begin), 0) < 0) {
            perror("write");
        }

        // Receive server response {4,1,NB_Clients}
        while(recv(client_socket_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}
        fprintf(stdout, "Thread %d : Received server BEGIN response\n", ct->id);

        // Verifies input format
        if (!(recv_buff[0] == 4 && recv_buff[1] == 1 && recv_buff[2] == count)) {
            fprintf(stdout, "Thread %d : Received wrong reponse format from server\n", ct->id);
            exit(0);
        }

        ////////////////
        // BEGIN DONE //
        ////////////////

        // CONF request
        int request_conf[2+num_resources];
        request_conf[0] = 1;
        request_conf[1] = num_resources;

        for(int i=0; i<num_resources; i++) {
            request_conf[i+2] = provisioned_resources[i];
        }

        // Send second request to server (CONF) {1,NB_ress,ress_1,ress_2,...,ress_n}
        if (send(client_socket_fd, request_conf, sizeof(request_conf), 0) < 0) {
            perror("send()");
        }

        // Receive server response {4,0}
        while(recv(client_socket_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}
        fprintf(stdout, "Thread %d : Received server CONF response\n", ct->id);

        // Verifies input format
        if (!(recv_buff[0] == 4 && recv_buff[1] == 0)) {
            fprintf(stdout, "Thread %d : Received wrong reponse format from server\n", ct->id);
            exit(0);
        }

        /////////////////
        /// CONF DONE ///
        /////////////////

        // Allocation des tableaux de ressources pour chaque thread (matrices)
        ressources_actual = malloc(sizeof(int*)*count);
        ressources_need = malloc(sizeof(int*)*count);
        last_request = malloc(sizeof(int*)*count);

        for (int i=0;i<count;i++) {
            ressources_actual[i] = malloc(sizeof(int)*num_resources);
            ressources_need[i] = malloc(sizeof(int)*num_resources);
            last_request[i] = malloc(sizeof(int)*num_resources);
        }

        // Random ressource de chaque type
        for(int i=0; i<count; i++) {
            for(int j=0; j<num_resources; j++) {
                int random = rand() % (provisioned_resources[j]+1);
                ressources_need[i][j] = random;
                ressources_actual[i][j] = 0;
                last_request[i][j] = 0;
            }
        }

        // Access variable
        begun = 1;

        // On ferme le socket
        close(client_socket_fd);

        // creation du socket client pour communiquer avec le serveur
        client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    }

    // Release lock
    pthread_mutex_unlock(&lock);

    // INIT requete
    int request_init[num_resources+3];
    request_init[0] = 2;
    request_init[1] = num_resources + 1;
    request_init[2] = ct->id;

    // Random ressources from ressources_need tab
    for(int i=0; i<num_resources; i++) {
        request_init[i+3] = ressources_need[ct->id][i];
    }

    // Connect all threads after BEGIN and CONF
    if(connect(client_socket_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("connect");
    }

    // Envoi request_init
    if (send(client_socket_fd, request_init, sizeof(request_init), 0) < 0) {
        perror("send()");
    } else {
        fprintf(stdout, "INIT %d : %d, %d, %d, %d, %d\n", ct->id, ressources_need[ct->id][0], ressources_need[ct->id][1], ressources_need[ct->id][2], ressources_need[ct->id][3], ressources_need[ct->id][4]);
    }

    // Receives server response
    while(recv(client_socket_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}

    // Verifies input format
    if (!(recv_buff[0] == 4 && recv_buff[1] == 0)) {
        fprintf(stdout, "Thread %d : Received wrong reponse format from server(INIT)\n", ct->id);
        exit(0);
    } else {
        count_dispatched++;
        initiated++;
    }

    // Closes socket
    close(client_socket_fd);

    // Lock to prevent clients to enter request zone while a thread has not been initiated
    while(initiated != count) {}

    int response = 4;

    for (unsigned int request_id = 0; request_id < num_request_per_client; request_id++)
    {
        // Incremente nb de requete total
        request_sent++;

        // creation du socket client pour communiquer avec le serveur
        client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);

        // MAYBE MONITEUR HERE

        // Connect to server
        if(connect(client_socket_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
            perror("connect()");
        }

        // Faire une random request
        response = send_request(ct->id, request_id, client_socket_fd, response);

        // Close socket
        close(client_socket_fd);

        // Faire quelque chose dependamment de la reponse
        switch (response) {
            case 4: 
                // Retrait des ressources pour ACK
                for (int i=0; i<num_resources; i++) {
                    ressources_actual[ct->id][i] -= last_request[ct->id][i];
                }
                break;
            case 5: 
                // WAIT pour WAIT
                sleep(1);
                break;
            case 6: 
                // ERR
                break;
        }

        // Simuler calcul
        sleep(0.1);
    }

    pthread_exit (NULL);
}


//
// Vous devez changer le contenu de cette fonction afin de régler le
// problème de synchronisation de la terminaison.
// Le client doit attendre que le serveur termine le traitement de chacune
// de ses requêtes avant de terminer l'exécution.
//
void
ct_wait_server ()
{
    // Free all data structures

    sleep(30);
}


// ##################DONE####################
void
ct_init (client_thread * ct)
{
    ct->id = count++;
}


// ##################DONE####################
void
ct_create_and_start (client_thread * ct)
{
    pthread_attr_init (&(ct->pt_attr));
    pthread_attr_setdetachstate(&(ct->pt_attr), PTHREAD_CREATE_DETACHED);
    pthread_create (&(ct->pt_tid), &(ct->pt_attr), &ct_code, ct);
}

// ##################DONE####################
// Affiche les données recueillies lors de l'exécution du
// serveur.
// La branche else ne doit PAS être modifiée.
//
void
st_print_results (FILE * fd, bool verbose)
{
    if (fd == NULL)
        fd = stdout;
    if (verbose)
    {
        fprintf (fd, "\n---- Résultat du client ----\n");
        fprintf (fd, "Requêtes acceptées: %d\n", count_accepted);
        fprintf (fd, "Requêtes wait : %d\n", count_on_wait);
        fprintf (fd, "Requêtes invalides: %d\n", count_invalid);
        fprintf (fd, "Clients : %d\n", count_dispatched);
        fprintf (fd, "Requêtes envoyées: %d\n", request_sent);
    }
    else
    {
        fprintf (fd, "%d %d %d %d %d\n", count_accepted, count_on_wait,
                 count_invalid, count_dispatched, request_sent);
    }
}
