//#define _XOPEN_SOURCE 700   /* So as to allow use of `fdopen` and `getline`.  */

#include "common.h"
#include "server_thread.h"

#include <netinet/in.h>
#include <netdb.h>

#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <time.h>

enum { NUL = '\0' };

enum {
    /* Configuration constants.  */
            max_wait_time = 30,
    server_backlog_size = 5
};

unsigned int server_socket_fd;

// Nombre de client enregistré.
int nb_registered_clients;

// Mutex booleans
pthread_mutex_t lock;

// Variable du journal.
// Nombre de requêtes acceptées immédiatement (ACK envoyé en réponse à REQ).
unsigned int count_accepted = 0;

// Nombre de requêtes acceptées après un délai (ACK après REQ, mais retardé).
unsigned int count_wait = 0;

// Nombre de requête erronées (ERR envoyé en réponse à REQ).
unsigned int count_invalid = 0;

// Nombre de clients qui se sont terminés correctement
// (ACK envoyé en réponse à CLO).
unsigned int count_dispatched = 0;

// Nombre total de requête (REQ) traités.
unsigned int request_processed = 0;

// Nombre de clients ayant envoyé le message CLO.
unsigned int clients_ended = 0;

typedef struct client {
    int id;
    bool closed;
    bool waiting;
    int *max;
    int *alloc;
    int *need;
    struct client *next;
} client;

struct banker {
    int *avail;
    client *clients;
    pthread_mutex_t mutex;
} banker;

// Ressources du serveur
int *available_ressources;
int *max_ressources;
int nb_ressources;

// Ressources pour chaque client
int **client_max;
int **client_actual;
int **client_needed;

// Function that updates needed ressources for a client
void update_needed(int client_id)
{
    for (int i=0; i<nb_ressources; i++) {
        client_needed[client_id][i] = client_max[client_id][i] - client_actual[client_id][i];
    }
}

client *
find_client(int id)
{
    client *c = banker.clients;
    while (c != NULL) {
        if (c->id == id)
            return c;
        c = c->next;
    }
    return NULL;
}

bool
is_empty(int *res)
{
    for (int i = 0; i < 5; i++)
        if (res[i] != 0)
            return false;
    return true;
}

void
st_init ()
{
    // Initialise le nombre de clients et ressources
    int expected_clients;
    nb_registered_clients = 0;

    // Buffer
    int recv_buff[64];

    // Socket to accept with its addr
    int client_sock_fd;
    struct sockaddr_in addr;
    int addr_len = sizeof(addr);

    // Accepts first client connection
    while((client_sock_fd = accept(server_socket_fd, (struct sockaddr *)&addr, (socklen_t *)&addr_len)) < 0) {}

    // banquier
    banker.avail = NULL;
    banker.clients = NULL;
    pthread_mutex_init(&banker.mutex, NULL);

    while(recv(client_sock_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}
    fprintf(stdout, "Server: Received BEGIN request from client\n");

    // Verifies input format
    if (!(recv_buff[0] == 0 && recv_buff[1] == 1 && recv_buff[2] > 0)) {
        fprintf(stdout, "Server: Wrong command format (BEGIN)\n");
        exit(0);
    }

    // Nombre de clients attendus au total
    expected_clients = recv_buff[2];

    pthread_mutex_lock(&banker.mutex);
    banker.avail = calloc(expected_clients, sizeof(int));
    pthread_mutex_unlock(&banker.mutex);

    // ack_1
    int ack_1[3] = {4,1,expected_clients};

    // Sends confirmation {4,1,NB_Clients}
    if (send(client_sock_fd, ack_1, sizeof(ack_1), 0) < 0) {
        perror("send()");
    }

    ////////////////
    // BEGIN DONE //
    ////////////////

    // Receives next command {1,NB_ress,ress_1,ress_2,...,ress_n}
    while(recv(client_sock_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}
    fprintf(stdout, "Server: Received CONF request from client\n");

    // Verifies input format
    if (!(recv_buff[0] == 1 && recv_buff[1] > 0)) {
        fprintf(stdout, "Server: Wrong command format (CONF)\n");
        exit(0);
    }

    // Nombre de ressources differentes du serveur
    nb_ressources = recv_buff[1];

    // ack_2
    int ack_2[2] = {4,0};

    // Sends confirmation {4,0}
    if (send(client_sock_fd, ack_2, sizeof(ack_2), 0) < 0) {
        perror("send()");
    }

    ////////////////
    // CONF DONE //
    ////////////////

    //
    // initialize data structures for banquier algorithm
    //

    // Allocation memoire des tableaux de ressources
    available_ressources = malloc(sizeof(int)*nb_ressources);
    max_ressources = malloc(sizeof(int)*nb_ressources);

    // Ressources du serveur (initiales et restantes)
    for(int i=0; i<nb_ressources; i++) {
        available_ressources[i] = recv_buff[i+2];
        max_ressources[i] = recv_buff[i+2];
    }

    // Allocation memoire hauteur matrice ressources des clients (maximum et actuelles)
    client_actual = malloc(sizeof(int*)*expected_clients);
    client_max = malloc(sizeof(int*)*expected_clients);
    client_needed = malloc(sizeof(int*)*expected_clients);

    // Rempli matrices des ressources actuelles des clients (0 partout)
    // Fait aussi les allocations memoire de la largeur des matrices
    for (int i=0;i<expected_clients;i++) {
        client_actual[i] = malloc(sizeof(int)*nb_ressources);
        client_max[i] = malloc(sizeof(int)*nb_ressources);
        client_needed[i] = malloc(sizeof(int)*nb_ressources);
        for (int j=0;j<nb_ressources;j++) {
            client_actual[i][j] = 0;
            client_needed[i][j] = 0;
        }
    }

    // Attend les connections client pour INIT
    while(nb_registered_clients != expected_clients) {
        // Reset socket
        client_sock_fd = -1;

        // Wait for connection
        while((client_sock_fd = accept(server_socket_fd, (struct sockaddr *)&addr, (socklen_t *)&addr_len)) < 0) {}

        // Attend INIT
        while(recv(client_sock_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}
        nb_registered_clients++;

        fprintf(stdout, "INIT request from client %d\n", recv_buff[2]);

        // ID client
        int client_id = recv_buff[2];

        // Gestion erreur
        if (recv_buff[0] != 2) {
            fprintf(stdout, "Wrong INIT request format\n");
            exit(0);
        }

        fprintf(stdout, "Ressources thread %d : %d, %d, %d, %d, %d\n", recv_buff[2], recv_buff[3], recv_buff[4], recv_buff[5], recv_buff[6], recv_buff[7]);


        // Alloue les ressources dans les tableaux
        for (int k=0; k<nb_ressources; k++) {
            client_max[client_id][k] = recv_buff[3+k];

            client *c = malloc(sizeof(client));
            c->id = client_id;
            c->closed = false;
            c->waiting = false;
            c->max = client_max[client_id][k];
            c->alloc = calloc(nb_ressources, sizeof(int));
            c->need = malloc(nb_ressources * sizeof(int));
            memcpy(c->need, client_max[client_id][k], nb_ressources * sizeof(int));
            fprintf(stdout, "CHILL\n");
        }


        // ack_3
        int ack_3[2] = {4,0};

        // Sending ACK to client
        if(send(client_sock_fd, ack_3, sizeof(ack_3), 0) < 0) {
            perror("send()");
        }

        // ferme socket pour en ouvrir un autre
        close(client_sock_fd);
    }

    /////////////////
    /// INIT DONE ///
    /////////////////
}

// banquier methodes
bool
res_more_than(int *arr1, int *arr2)
{
    for (int i = 0; i < nb_registered_clients; i++)
        if (arr1[i] > arr2[i])
            return true;
    return false;
}

void
allocate_req(int *req, int *avail, int *alloc, int *need)
{
    for (int i = 0; i < nb_registered_clients; i++) {
        avail[i] -= req[i];
        alloc[i] += req[i];
        need[i] -= req[i];
    }
}

void
deallocate_req(int *req, int *avail, int *alloc, int *need)
{
    for (int i = 0; i < nb_registered_clients; i++) {
        avail[i] += req[i];
        alloc[i] -= req[i];
        need[i] += req[i];
    }
}

bool
is_safe(int num_clients, int *avail, client * clients)
{
    bool finish[num_clients];
    memset(finish, false, num_clients * sizeof(bool));

    int work[nb_registered_clients];
    memcpy(work, avail, nb_registered_clients * sizeof(int));

    int counter = num_clients;
    while (counter) {
        bool safe = false;

        client *c = clients;
        for (int i = 0; i < num_clients; i++) {
            if (!finish[i]) {
                int j;
                for (j = 0; j < nb_registered_clients; j++)
                    if (c->need[j] > work[j])
                        break;

                if (j == nb_registered_clients) {
                    for (int k = 0; k < nb_registered_clients; k++)
                        work[k] += c->alloc[k];

                    counter--;
                    finish[i] = true;
                    safe = true;
                }
            }

            c = c->next;
        }

        if (!safe)
            return false;
    }

    return true;
}

void
st_process_requests (server_thread * st, int socket_fd)
{
    // Buffer to receive inputs via socket
    int recv_buff[64];

    // Dummy response
    int response[2] = {4,0};

    // Receive command
    while(recv(socket_fd, recv_buff, sizeof(recv_buff), 0) < 0) {}

    // ID
    int client_id = recv_buff[2];

    // Feedback
    fprintf(stdout, "Client %d asks : %d, %d, %d, %d, %d\n", client_id, recv_buff[3], recv_buff[4], recv_buff[5], recv_buff[6], recv_buff[7]);

    // Mutex
    pthread_mutex_t mutex_nb_registered_clients = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mutex_count_accepted = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mutex_count_wait = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mutex_count_invalid = PTHREAD_MUTEX_INITIALIZER;

    client *c;
    pthread_mutex_lock(&banker.mutex);
    if (!(c = find_client(client_id))) {
        pthread_mutex_unlock(&banker.mutex);
        pthread_mutex_lock(&mutex_count_invalid);
        count_invalid++;
        pthread_mutex_unlock(&mutex_count_invalid);
        fprintf(stdout,"Serveur ERR : REQ before INIT\n");
        response[0] = 8;
    }

    if (c->closed) {
        pthread_mutex_unlock(&banker.mutex);
        pthread_mutex_lock(&mutex_count_invalid);
        count_invalid++;
        pthread_mutex_unlock(&mutex_count_invalid);
        fprintf(stdout,"Serveur ERR : REQ after CLO\n");
        response[0] = 8;
    }
    pthread_mutex_unlock(&banker.mutex);

    ///////////////////
    // Algo banquier //
    ///////////////////
    int req[5] = {recv_buff[3], recv_buff[4], recv_buff[5], recv_buff[6], recv_buff[7]};

    // more than needed
    pthread_mutex_lock(&banker.mutex);
    if (res_more_than(req, c->need)) {
        pthread_mutex_unlock(&banker.mutex);
        pthread_mutex_lock(&mutex_count_invalid);
        count_invalid++;
        pthread_mutex_unlock(&mutex_count_invalid);
        fprintf(stdout,"Serveur ERR : requesting more than needed\n");
        response[0] = 8;
        response[1] = 1;
    }

    // more than allocated
    for (int i = 0; i < 5; i++) {
        if (req[i] + c->alloc[i] < 0) {
            pthread_mutex_unlock(&banker.mutex);
            pthread_mutex_lock(&mutex_count_invalid);
            count_invalid++;
            pthread_mutex_unlock(&mutex_count_invalid);
            fprintf(stdout,"Serveur ERR : freeing more than allocated\n");
            response[0] = 8;
            response[1] = 1;
        }
    }

    // more than available
    if (res_more_than(req, banker.avail)) {
        c->waiting = true;
        pthread_mutex_unlock(&banker.mutex);
        response[0] = 5;//wait
        response[1] = 1;
    }

    // Race condition : lecture et changement dans les structures
    pthread_mutex_lock(&lock);

    // banquier
    allocate_req(req, banker.avail, c->alloc, c->need);
    pthread_mutex_lock(&mutex_nb_registered_clients);
    if (!is_safe(nb_registered_clients, banker.avail, banker.clients)) {
        pthread_mutex_unlock(&mutex_nb_registered_clients);
        deallocate_req(req, banker.avail, c->alloc, c->need);
        c->waiting = true;
        pthread_mutex_unlock(&banker.mutex);
        response[0] = 5;//wait
        response[1] = 1;
    }
    pthread_mutex_unlock(&mutex_nb_registered_clients);

    if (c->waiting) {
        c->waiting = false;
        pthread_mutex_unlock(&banker.mutex);
        pthread_mutex_lock(&mutex_count_wait);
        count_wait++;
        pthread_mutex_unlock(&mutex_count_wait);
    } else {
        pthread_mutex_unlock(&banker.mutex);
        pthread_mutex_lock(&mutex_count_accepted);
        count_accepted++;
        pthread_mutex_unlock(&mutex_count_accepted);
    }

    // Unlock mutex
    pthread_mutex_unlock(&lock);


    // Send response to client
    if (send(socket_fd, response, sizeof(response), 0) < 0) {
      perror("send()");
    }
}

void
st_signal ()
{
    // Free all data structures
    
    sleep(30);
}

void *
st_code (void *param)
{
    server_thread *st = (server_thread *) param;

    struct sockaddr_in thread_addr;
    socklen_t socket_len = sizeof (thread_addr);
    int thread_socket_fd = -1;
    int end_time = time (NULL) + max_wait_time;

    // Boucle jusqu'à ce que `accept` reçoive la première connection.
    while (thread_socket_fd < 0)
    {
        thread_socket_fd =
                accept (server_socket_fd, (struct sockaddr *) &thread_addr,
                        &socket_len);

        if (time (NULL) >= end_time)
        {
            break;
        }
    }

    // Boucle de traitement des requêtes.
    while (accepting_connections)
    {
        if (!nb_registered_clients && time (NULL) >= end_time)
        {
            fprintf (stderr, "Time out on thread %d.\n", st->id);
            pthread_exit (NULL);
        }

        if (thread_socket_fd > 0)
        {
            st_process_requests (st, thread_socket_fd);
            close(thread_socket_fd);
            end_time = time (NULL) + max_wait_time;
        }

        thread_socket_fd =
                accept (server_socket_fd, (struct sockaddr *) &thread_addr,
                        &socket_len);
    }
    return NULL;
}


//
// Ouvre un socket pour le serveur.
//
void
st_open_socket (int port_number)
{
    server_socket_fd = socket (AF_INET, SOCK_STREAM /*| SOCK_NONBLOCK*/, 0);
    if (server_socket_fd <= 0)
        perror ("ERROR opening socket");

    if (setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEPORT, &(int){ 1 }, sizeof(int)) < 0) {
        perror("setsockopt()");
        exit(1);
    }

    struct sockaddr_in serv_addr;
    memset (&serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    serv_addr.sin_port = htons(port_number);

    if (bind(server_socket_fd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        perror ("ERROR on binding");
    }

    listen (server_socket_fd, server_backlog_size);
}


//
// Affiche les données recueillies lors de l'exécution du
// serveur.
// La branche else ne doit PAS être modifiée.
//
void
st_print_results (FILE * fd, bool verbose)
{
    if (fd == NULL) fd = stdout;
    if (verbose)
    {
        fprintf (fd, "\n---- Résultat du serveur ----\n");
        fprintf (fd, "Requêtes acceptées: %d\n", count_accepted);
        fprintf (fd, "Requêtes : %d\n", count_wait);
        fprintf (fd, "Requêtes invalides: %d\n", count_invalid);
        fprintf (fd, "Clients : %d\n", count_dispatched);
        fprintf (fd, "Requêtes traitées: %d\n", request_processed);
    }
    else
    {
        fprintf (fd, "%d %d %d %d %d\n", count_accepted, count_wait,
                 count_invalid, count_dispatched, request_processed);
    }
}
